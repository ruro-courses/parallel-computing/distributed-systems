#!/bin/sh
set -euo pipefail
cd "$(dirname "$0")"

runtime/build_ulfm.sh
runtime/build_project.sh
report/build_report.sh
