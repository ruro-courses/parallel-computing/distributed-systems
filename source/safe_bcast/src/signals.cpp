#include "signals.hpp"

#include "cerrno"


std::future<void> make_watcher(sigset_t set)
{
    return std::async(
        std::launch::async,
        [=]()
        {
            while (sigwaitinfo(&set, nullptr) == -1)
            {
                if (errno != EINTR)
                {
                    throw std::runtime_error(
                        "sigwaitinfo failed with errno " + std::to_string(errno)
                    );
                }
            }
        }
    );
}

SignalWatcher::SignalWatcher(int sig_no) : set{}, old{}, watcher{}, last_state{}
{
    if (sigemptyset(&set) ||
        sigaddset(&set, sig_no))
    { throw std::runtime_error("Couldn't get signal mask"); }

    if (sigprocmask(SIG_BLOCK, &set, &old))
    { throw std::runtime_error("Couldn't block specified signal"); }

    watcher = make_watcher(set);
}

SignalWatcher::~SignalWatcher()
{ sigprocmask(SIG_SETMASK, &old, nullptr); }

bool SignalWatcher::try_receive()
{
    if (last_state)
    { return true; }

    using namespace std::literals::chrono_literals;
    if (watcher.wait_for(0s) == std::future_status::ready)
    {
        last_state = true;
        watcher = make_watcher(set);
        return true;
    }
    return false;
}

void SignalWatcher::clear()
{ last_state = false; }
