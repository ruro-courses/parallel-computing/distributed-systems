#include "exceptions.hpp"

#include <cstdarg>
#include <string>
#include <mpi-ext.h>


MPI_Comm_errhandler_function throw_on_error;

void throw_on_error(MPI_Comm *, int *ret, ...)
{
    va_list args;
    va_start(args, ret);
    auto message = va_arg(args, const char *);
    auto nul = va_arg(args, void *);
    va_end(args);

    char description[MPI_MAX_ERROR_STRING];
    int result_len;
    MPI_Error_string(*ret, description, &result_len);

    if (nul != nullptr)
    {
        throw std::runtime_error(
            "Unsupported error handler signature, expected "
            "(MPI_Comm *, int *, const char *, nullptr)"
        );
    }
    switch (*ret)
    {
#if defined(MPIX_ERR_PROC_FAILED) && \
    defined(MPIX_ERR_PROC_FAILED_PENDING) && \
    defined(MPIX_ERR_REVOKED)
        case MPIX_ERR_PROC_FAILED: [[fallthrough]]; // NOLINT(bugprone-branch-clone)
        case MPIX_ERR_PROC_FAILED_PENDING: [[fallthrough]];
        case MPIX_ERR_REVOKED:
        { throw ULFMException(*ret, message, description); }
#endif
        default:
        { throw MPIException(*ret, message, description); }
    }
}

const char *MPIException::what() const noexcept
{ return _what.c_str(); }

using namespace std::string_literals;

MPIException::MPIException(int _errcode, const char *_msg, const char *_des)
    : errcode(_errcode), _what(
    "An unhandled MPI error occurred: [error code: " +
    std::to_string(_errcode) + "]" +
    (
        _msg
        ? (" (message: "s + _msg + ")")
        : ""
    ) +
    (
        _des
        ? (" <description: "s + _des + ">")
        : ""
    )
)
{}
