#include "faults.hpp"
#include "exceptions.hpp"

#include <mpi-ext.h>
#include <cstring>


constexpr auto int_bits = 8 * sizeof(int);

bool get_status(std::vector<unsigned int> &status, int index)
{ return (status[index / int_bits] & (1u << (index % int_bits))); }

void agree_reduce_or(std::vector<unsigned int> &v, MPI_Comm world)
{
    // Agree reduce using the bitwise OR operation
    for (auto &flag : v)
    {
        int _flag;
        flag = ~flag;
        memcpy(&_flag, &flag, sizeof(int));
        MPIX_Comm_agree(world, &_flag);
        memcpy(&flag, &_flag, sizeof(int));
        flag = ~flag;
    }
}

void FaultWatcher::recover_and_shrink()
{
    // Update the world status to exclude all dead processes
    MPI_Comm new_world;
    MPIX_Comm_revoke(*current_world);
    MPIX_Comm_failure_ack(*current_world);

    auto new_status = std::vector<unsigned int>(status.size());
    new_status[original_rank / int_bits] |= 1u << (original_rank % int_bits);
    agree_reduce_or(new_status, *current_world);

    MPIX_Comm_shrink(*current_world, &new_world);
    if (*current_world != MPI_COMM_WORLD)
    { MPI_Comm_free(current_world); }
    *current_world = new_world;

    // Update the global comm information
    mpi_common_cerr.reindex(new_world);
    mpi_master_cout.reindex(new_world);

    // Poll all the processes about their health status
    MPI_Barrier(new_world);
    for (int i = 0; i < original_size; i++)
    {
        if (get_status(status, i) && !get_status(new_status, i))
        {
            current_ranks[i] = -1;
            mpi_master_cout
                << "Process with id " << i
                << " died. ULFM recovery successful.\n" << sync_done;
        }
        else
        { current_ranks[i] = static_cast<int>(current_ranks[i]) < 0 ? current_ranks[i] : 0; }
    }

    std::swap(status, new_status);

    int current_rank;
    MPI_Comm_rank(new_world, &current_rank);
    MPI_Comm_size(new_world, &current_size);
    current_ranks[original_rank] = current_rank;
    agree_reduce_or(current_ranks, new_world);
    MPI_Barrier(new_world);
}

bool FaultWatcher::is_alive(int index)
{ return get_status(status, index); }

FaultWatcher::FaultWatcher(MPI_Comm *_comm)
    : original_size(), original_rank(), current_size(), current_world(_comm), status(), current_ranks()
{
    MPI_Comm_rank(*current_world, &original_rank);
    MPI_Comm_size(*current_world, &original_size);

    current_size = original_size;
    current_ranks.resize(original_size, 0);
    current_ranks[original_rank] = original_rank;
    agree_reduce_or(current_ranks, *current_world);

    status.resize(1 + (original_size - 1) / int_bits);
    status[original_rank / int_bits] |= 1u << (original_rank % int_bits);
    agree_reduce_or(status, *current_world);
}

void FaultWatcher::try_execute(const std::function<void()> &f)
{
    try
    { f(); }
    catch (ULFMException &e)
    { recover_and_shrink(); }
}

int FaultWatcher::get_current_rank(int id)
{
    int rank = current_ranks[id];
    if (rank < 0)
    { throw std::runtime_error("Attempted to get current rank for dead process."); }
    return rank;
}

int FaultWatcher::get_proc_size()
{ return original_size; }

int FaultWatcher::get_self_id()
{ return original_rank; }

int FaultWatcher::get_current_id(int rank)
{
    auto id = std::find(current_ranks.begin(), current_ranks.end(), rank);
    if (id == current_ranks.end() || rank < 0)
    { throw std::runtime_error("Attempted to get current id for dead process."); }
    return id - current_ranks.begin();
}
