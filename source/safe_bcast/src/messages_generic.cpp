#include "messages.hpp"

#include <random>


GenericMessage::GenericMessage()
    : ltime_message_sent{-1}
{}

PayloadMessage::PayloadMessage()
    : GenericMessage{}, message_id{}, payload_contents{}
{
    std::random_device r;
    std::default_random_engine generator{r()};
    std::uniform_int_distribution<int> id_distribution(1);
    std::uniform_int_distribution<char> msg_distribution('a', 'z');
    message_id = id_distribution(generator);
    for (char &c : payload_contents)
    { c = msg_distribution(generator); }
    payload_contents[payload_size - 1] = '\0';
}

MessageMetadata::MessageMetadata(int _sender_id, int _ltime_sent)
    : sender_id{_sender_id},
      message_priority{std::max(ltime_current, _ltime_sent)},
      delivered{false}
{ ltime_current = 1 + message_priority; }

AckReceivedMessage::AckReceivedMessage(int _sender_id, int _message_id, int _ltime_received_priority)
    : GenericMessage{}, sender_id{_sender_id}, message_id{_message_id}, ltime_received_priority{_ltime_received_priority}
{
}

AckDeliveredMessage::AckDeliveredMessage(int _message_id, int _ltime_final_priority)
    : GenericMessage{}, message_id{_message_id}, ltime_final_priority{_ltime_final_priority}
{}

StatusRequestMessage::StatusRequestMessage(int _message_id, int _rebroadcast_id)
    : GenericMessage{}, message_id(_message_id), rebroadcast_id{_rebroadcast_id}
{}
