#include "messages.hpp"
#include "globals.hpp"


void send_recipient(AnyMessage m, int dest_id)
{
    std::visit(
        [&](auto &mv)
        { mv.ltime_message_sent = ltime_current++; },
        m
    );
    timed_Bsend(&m, sizeof(m), fw->get_current_rank(dest_id), 0, comm_world);
}

void send_all(AnyMessage m)
{
    for (int i = 0; i < fw->get_proc_size(); ++i)
    {
        if (fw->is_alive(i))
        { send_recipient(m, i); }

        using namespace std::literals::chrono_literals;
        std::this_thread::sleep_for(0.1s);
    }
}

void MessageWatcher::send(PayloadMessage &m)
{
    sent_statuses.emplace(m.message_id, fw->get_proc_size());
    send_all(m);
}

void MessageWatcher::send(AckReceivedMessage &m)
{ send_recipient(m, m.sender_id); }

void MessageWatcher::send(AckDeliveredMessage &m)
{ send_all(m); }

void MessageWatcher::send(StatusRequestMessage &m)
{
    if (m.rebroadcast_id < 0)
    { send_all(m); }
    else
    { send_recipient(m, m.rebroadcast_id); }
}
