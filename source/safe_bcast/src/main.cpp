#include "mpi_common.hpp"

#include "globals.hpp"
#include "exceptions.hpp"


[[noreturn]] void work_loop()
{
    // Loop forever
    while (true)
    {
        // Automatically handle any possible faults
        fw->try_execute(
            []()
            {
                // Received SIGALRM, generate new message and broadcast it
                if (sw->try_receive())
                {
                    PayloadMessage pm{};
                    mpi_common_cerr
                        << "Process " << fw->get_self_id()
                        << " received SIGALRM and will now send a payload with contents: ["
                        << pm.payload_contents << "].\n" << sync_done;
                    mw->send(pm);
                    return sw->clear();
                }

                // Received a message, process it and maybe respond
                if (auto event = mw->try_receive(); event)
                {
                    std::visit(
                        [](auto &record)
                        { mw->receive(record); },
                        event.value()
                    );
                    return mw->clear();
                }

                if (auto payload = mw->try_accept_pending(); payload)
                {
                    auto &[meta, data] = payload.value();
                    mpi_common_cerr
                        << "Process " << fw->get_self_id()
                        << " accepted a payload from "
                        << meta.sender_id << " with contents: ["
                        << data.payload_contents << "] and priority "
                        << meta.message_priority << ".\n" << sync_done;
                }

                // No events received, yield to reduce busy looping
                std::this_thread::yield();
            }
        );
    }
}

int main(int argc, char *argv[])
{
    // Listen for SIGALRM
    // (this needs to be before init due to MPI spawning it's own threads)
    auto signals = SignalWatcher(SIGALRM);

    // Throw ULFM exceptions instead of crashing
    init(&argc, &argv, throw_on_error);

    // Monitor processes for health, automatically kick
    // dead processes from comm_world
    auto faults = FaultWatcher();

    // Receive arbitrary messages
    auto messages = MessageWatcher();

    fw = &faults;
    sw = &signals;
    mw = &messages;

    // Allocate an arena buffer, that will be used for the buffered send
    // We can't know, how many broadcasts may be active at the same time.
    // Here, we are allocating enough to support up to 4 (in the worst case)
    Arena msg_arena = Arena(sizeof(AnyMessage), 4 * fw->get_proc_size());

    work_loop();
}
