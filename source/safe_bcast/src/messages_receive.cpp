#include "messages.hpp"

#include "globals.hpp"


void notify(const char *message_type, int sender_id, int priority)
{
    mpi_common_cerr
        << "Process " << fw->get_self_id()
        << " received a message of type " << message_type
        << " from " << sender_id
        << " with priority " << priority
        << ".\n" << sync_done;
}

std::optional<RecordT<PayloadMessage>> MessageWatcher::try_accept_pending()
{
    if (!pending_payloads.size())
    { return {}; }

    //- Упорядочивает все буферизованные сообщения по возрастанию их приписанных приоритетов.
    std::sort(
        pending_payloads.begin(),
        pending_payloads.end(),
        [&](int left, int right)->bool
        {
            auto const &[meta_l, payload_l] = received_payloads.at(left);
            auto const &[meta_r, payload_r] = received_payloads.at(right);
            (void) payload_l;
            (void) payload_r;
            return meta_l.message_priority < meta_r.message_priority;
        }
    );

    //- Если первое сообщение в очереди отмечено как "доставленное", то оно будет обрабатываться как окончательно полученное.
    auto index = pending_payloads.begin();
    auto index_v = *index;
    auto &record_v = received_payloads.at(index_v);
    auto &[meta_v, payload_v] = record_v;
    if (meta_v.delivered)
    {
        pending_payloads.erase(index);
        return record_v;
    }

    //Если получатель обнаружит, что он имеет сообщение с пометкой "недоставленное", отправитель которого сломался, то он для завершения выполнения протокола осуществляет следующие два шага в качестве координатора.
    if (fw->is_alive(meta_v.sender_id) || sent_statuses.count(payload_v.message_id))
    { return {}; }

    sent_statuses.emplace(payload_v.message_id, fw->get_proc_size());
    StatusRequestMessage m{payload_v.message_id};
    mw->send(m);
    return {};
    //- Опрашивает всех получателей о статусе этого сообщения.
    //    При этом получатель может ответить одним из трех способов:
    //    - Сообщение отмечено как "недоставленное" и ему приписан такой-то приоритет.
    //    - Сообщение отмечено как "доставленное" и имеет такой-то окончательный приоритет.
    //    - Он не получал это сообщение.
    //- Получив все ответы координатор выполняет следующие действия:
    //    - Если сообщение у какого-то получателя помечено как "доставленное", то его окончательный приоритет рассылается всем.
    //        (Получив это сообщение каждый процесс выполняет шаги фазы 2).
    //    - Иначе координатор заново начинает весь протокол с фазы 1 в роли отправителя.
    //        (Повторная посылка сообщения с одинаковым приоритетом не должна вызывать коллизий).
}

void MessageWatcher::receive(RecordT<PayloadMessage> &rec)
{
    auto &[meta, message] = rec;
    notify("payload", meta.sender_id, meta.message_priority);

    // Record the message as pending, if it wasn't already delivered
    if (received_payloads.count(message.message_id))
    {
        // If it was already delivered, update the current message
        // so that the ack will be sent to the new sender
        auto &[r_meta, r_message] = received_payloads.at(message.message_id);
        if (r_meta.delivered)
        {
            meta.delivered = true;
            meta.message_priority = std::max(meta.message_priority, r_meta.message_priority);
        }
        r_meta = meta;
        r_message = message;
        (void) r_meta;
        (void) r_message;
        return;
    }
    else
    { received_payloads.emplace(message.message_id, std::move(rec)); }
    pending_payloads.push_back(message.message_id);

    // Inform sender, that we received the message and what priority did it get
    AckReceivedMessage ack{meta.sender_id, message.message_id, meta.message_priority};
    mw->send(ack);
}

void MessageWatcher::receive(RecordT<AckReceivedMessage> &rec)
{
    auto &[meta, message] = rec;
    notify("ack_received", meta.sender_id, message.ltime_received_priority);

    if (message.sender_id != fw->get_self_id() || !sent_statuses.count(message.message_id))
    { throw std::runtime_error("Received a `received` ack for an unknown message."); }

    // Record new status (don't lower the priority,
    // if an old message gets accidentally received.
    auto &status = sent_statuses.at(message.message_id);
    if (status[meta.sender_id] == 0)
    { status[meta.sender_id] = message.ltime_received_priority; }
    else
    { status[meta.sender_id] = std::max(status[meta.sender_id], message.ltime_received_priority); }

    // Find the process with the max priority
    int max_priority = message.ltime_received_priority;
    int max_proc_id = meta.sender_id;
    // And also check, if someone missed the payload
    bool missing_payloads = false;
    for (int i = 0; i < fw->get_proc_size(); i++)
    {
        // Ignore dead processes
        if (!fw->is_alive(i))
        { continue; }

        if (status[i] > 0) // Find the maximum priority
        {
            if (status[i] > max_priority)
            {
                max_proc_id = i;
                max_priority = status[i];
            }
        }
        else if (status[i] < 0) // There is some process, which didn't receive the message
        { missing_payloads = true; }
        else // There is still some process, which didn't ack, do nothing
        { return; }
    }

    // This shouldn't happen, because AckReceivedMessage was just received
    if (max_priority < 0)
    { throw std::runtime_error("Somebody asked about a message, that didn't exist."); }

    if (missing_payloads)
    {
        // Ask the process with the highest priority to rebroadcast the payload
        StatusRequestMessage req{message.message_id, max_proc_id};
        mw->send(req);
    }
    else
    {
        // Notify everyone that the message was delivered correctly
        AckDeliveredMessage ack{message.message_id, max_priority};
        mw->send(ack);
    }
}

void MessageWatcher::receive(RecordT<AckDeliveredMessage> &rec)
{
    auto &[ack_meta, ack] = rec;
    notify("ack_delivered", ack_meta.sender_id, ack.ltime_final_priority);

    if (received_payloads.count(ack.message_id))
    {
        auto &[meta, payload] = received_payloads.at(ack.message_id);
        (void) payload;

        // If we received a delivery notification for a message, we already consider delivered, do nothing
        if (!meta.delivered)
        {
            // Done. The message was delivered successfully.
            // It will be collected on the appropriate try_accept_pending
            meta.message_priority = ack.ltime_final_priority;
            meta.delivered = true;
            (void) meta;
        }
    }
    else
    { throw std::runtime_error("Received a `delivered` ack for an unknown message."); }
}

void MessageWatcher::receive(RecordT<StatusRequestMessage> &rec)
{
    auto &[s_meta, message] = rec;
    notify("status_request", s_meta.sender_id, message.rebroadcast_id);

    if (message.rebroadcast_id < 0)
    {
        // Check, if we have received the message
        // If not, respond with priority -1, otherwise with the current priority
        AckReceivedMessage ack{
            s_meta.sender_id, message.message_id,
            received_payloads.count(message.message_id)
            ? received_payloads.at(message.message_id).first.message_priority
            : -1
        };
        mw->send(ack);
    }
    else
    {
        auto[p_meta, payload] = received_payloads.at(message.message_id);
        (void) p_meta;
        mw->send(payload);
    }
}
