#include "messages.hpp"

#include "mpi_common.hpp"
#include "globals.hpp"


int ltime_current = 1;

MessageWatcher::MessageWatcher(int _source, int _tag, MPI_Comm *_comm)
    : source(_source), tag(_tag), comm(_comm), last_status{}, received_payloads{}, pending_payloads{}, sent_statuses{}
{}

std::optional<AnyRecord> MessageWatcher::try_receive()
{
    if (last_status)
    { return last_status; }

    int flag;
    MPI_Status status;
    MPI_Iprobe(source, tag, *comm, &flag, &status);
    if (!flag)
    { return {}; }
    else
    {
        AnyMessage buf;
        timed_Recv(&buf, sizeof(buf), status.MPI_SOURCE, status.MPI_TAG, *comm);
        last_status = std::visit(
            [&](auto &message)->AnyRecord
            {
                using message_type = expr_type<decltype(message)>;
                return RecordT<message_type>{
                    MessageMetadata{
                        fw->get_current_id(status.MPI_SOURCE),
                        message.ltime_message_sent
                    },
                    message
                };
            }, buf
        );
        return last_status;
    }
}

void MessageWatcher::clear()
{ last_status = {}; }
