#pragma once


#include <csignal>
#include <future>


class SignalWatcher
{
public:
    explicit SignalWatcher(int sig_no);
    ~SignalWatcher();
    bool try_receive();
    void clear();
private:
    sigset_t set, old;
    std::future<void> watcher;
    bool last_state;
};
