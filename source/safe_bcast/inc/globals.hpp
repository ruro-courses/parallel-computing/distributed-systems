#pragma once


#include <signals.hpp>
#include <faults.hpp>
#include <messages.hpp>


extern SignalWatcher *sw;
extern FaultWatcher *fw;
extern MessageWatcher *mw;
