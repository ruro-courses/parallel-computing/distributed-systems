#pragma once


#include "mpi_common.hpp"

#include <optional>
#include <variant>
#include <future>
#include <unordered_map>


extern int ltime_current;


// Base message
class GenericMessage
{
public:
    explicit GenericMessage();
    int ltime_message_sent;
};


// Actual message with meaningful data
class PayloadMessage : public GenericMessage
{
public:
    static constexpr auto payload_size = 16;

    explicit PayloadMessage();
    int message_id;
    char payload_contents[payload_size];
};


// An acknowledgement response, after receiving
// a PayloadMessage or StatusRequestMessage
class AckReceivedMessage : public GenericMessage
{
public:
    explicit AckReceivedMessage(int _sender_id, int _message_id, int _ltime_received_priority);
    int sender_id;
    int message_id;
    int ltime_received_priority;
};


// An acknowledgement response, after collecting
// AckReceivedMessage from all intended recipients
class AckDeliveredMessage : public GenericMessage
{
public:
    explicit AckDeliveredMessage(int _message_id, int _ltime_final_priority);
    int message_id;
    int ltime_final_priority;
};


// A request to resend the AckReceivedMessage response
// this message only appears, if the original sender died
class StatusRequestMessage : public GenericMessage
{
public:
    explicit StatusRequestMessage(int _message_id, int _rebroadcast_id = -1);
    int message_id;
    int rebroadcast_id;
};


class MessageMetadata
{
public:
    explicit MessageMetadata(int _sender_id, int _ltime_sent);
    int sender_id;
    int message_priority;
    bool delivered;
};


template<typename T>
using RecordT = std::pair<MessageMetadata, T>;

using AnyMessage = std::variant<
    PayloadMessage,
    AckReceivedMessage,
    AckDeliveredMessage,
    StatusRequestMessage
>;

using AnyRecord = std::variant<
    RecordT<PayloadMessage>,
    RecordT<AckReceivedMessage>,
    RecordT<AckDeliveredMessage>,
    RecordT<StatusRequestMessage>
>;


class MessageWatcher
{
public:
    explicit MessageWatcher(int _source = MPI_ANY_SOURCE, int _tag = MPI_ANY_TAG, MPI_Comm *_comm = &comm_world);
    std::optional<AnyRecord> try_receive();
    std::optional<RecordT<PayloadMessage>> try_accept_pending();

    void receive(RecordT<PayloadMessage> &rec);
    void receive(RecordT<AckReceivedMessage> &rec);
    void receive(RecordT<AckDeliveredMessage> &rec);
    void receive(RecordT<StatusRequestMessage> &rec);

    void send(PayloadMessage &m);
    void send(AckReceivedMessage &m);
    void send(AckDeliveredMessage &m);
    void send(StatusRequestMessage &m);

    void clear();
private:
    int source, tag;
    MPI_Comm *comm;
    std::optional<AnyRecord> last_status;

    using MessageStatus = std::vector<int>;
    using PendingStatus = std::vector<int>;

    std::unordered_map<int, RecordT<PayloadMessage>> received_payloads;
    PendingStatus pending_payloads;
    std::unordered_map<int, MessageStatus> sent_statuses;
};
