#pragma once


#include <mpi.h>
#include <stdexcept>


void throw_on_error(MPI_Comm *, int *ret, ...);


class MPIException : public std::exception
{
public:
    int errcode;
    [[nodiscard]] const char *what() const noexcept override;
    explicit MPIException(int _errcode, const char *_msg = nullptr, const char *_des = nullptr);
private:
    std::string _what;
};


class ULFMException : public MPIException
{
    using MPIException::MPIException;
};
