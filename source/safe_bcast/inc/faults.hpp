#pragma once


#include "mpi_common.hpp"

#include <tuple>
#include <vector>
#include <functional>


void agree_reduce_or(std::vector<unsigned int> &v, MPI_Comm world);


class FaultWatcher
{
public:
    explicit FaultWatcher(MPI_Comm *_comm = &comm_world);
    void try_execute(const std::function<void()> &f);
    bool is_alive(int index);
    int get_current_rank(int id);
    int get_current_id(int rank);
    int get_proc_size();
    int get_self_id();

private:
    int original_size, original_rank;
    int current_size;
    MPI_Comm *current_world;

    std::vector<unsigned int> status;
    std::vector<unsigned int> current_ranks;
    void recover_and_shrink();
};

