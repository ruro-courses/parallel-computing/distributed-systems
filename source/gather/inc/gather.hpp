#pragma once


#include "mpi_common.hpp"
#include "grid.hpp"


void gather(const void *sendbuf, void *recvbuf, int esize, Grid grid);
