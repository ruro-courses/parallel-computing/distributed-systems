#pragma once


#include <mpi.h>
#include <future>


class Grid
{
public:
    Grid() = delete;
    Grid(int h, int w);
    Grid(const Grid &grid) noexcept;
    ~Grid();
    Grid &operator=(Grid &&src) noexcept;

    [[nodiscard]] Grid transpose() const noexcept;
    [[nodiscard]] Grid shift_view(int dx, int dy, int dh, int dw) const noexcept;

    void Bsend(void *buf, int size, int direction) const;
    void Recv(void *buf, int size, int direction) const;
    std::future<void> Irecv(void *buf, int size, int direction) const;

    int comm_rank = {};
    int comm_size = {};
    int coords[2] = {};
    int shape[2] = {};
    int dims[2] = {};
    MPI_Comm grid_comm = nullptr;
private:
    bool view = true;
};
