#include "gather.hpp"
#include <algorithm>
#include <cstring>


void *at(Buffer &location, int idx, int esize)
{ return location.data() + esize * idx; }

void copy_at(Buffer &location, int idx, const void *data, int esize)
{ memcpy(at(location, idx, esize), data, esize); }

void transpose_memcpy(void *out_buf, Buffer &inp, int esize, int h, int w)
{
    auto out = static_cast<char *>(out_buf);
    for (int i = 0; i < w; i++)
    {
        for (int j = 0; j < h; j++)
        {
            const auto ij = i * h + j;
            const auto ji = j * w + i;
            memcpy(out + esize * ji, at(inp, ij, esize), esize);
        }
    }
}

void fix_merge_order(Buffer &local_buf, int esize, const Grid &grid)
{
    const auto[h, w] = grid.shape;
    const auto block_size = (w - 1) * (h - 1);
    // The input data has the following layout:
    // [c, b0, b1, b2...bk, p0, p1 p2...ps]

    // We want to interleave the `b` and `p` parts like this:
    // [    c,   b0,         b1,       ...    b( w-2), b( w-1),
    //     p0,   bw,     b( w+1),      ...    b(2w-2), b(2w-1),
    //     p1,  b2w,     b(2w+1),      ...    b(3w-2), b(3w-1),
    //                ...
    //  p(h-2), b(h-1)w, b((h-1)w+1),  ...    b(k-1),  bk,
    //  p(h-1), ph,      p(h+1),       ...    p(s-1),  ps      ]

    // Luckily, this can be accomplished simply by rotating the segments
    // of the array. For example, the first rotation will take the segment
    // [b0, b1, b2, ... bk, p0] and rotate it to [p0, b0, b1, b2, ... bk]
    // The next iteration will take the segment [bw, bw+1, ... bk, p1]
    // and rotate it to [p1, bw, bw+1, ... bk]
    auto b_data = static_cast<char *>(at(local_buf, 1, esize));
    auto p_data = static_cast<char *>(at(local_buf, 1 + block_size, esize));
    b_data += (w - 1) * esize;
    while (b_data < p_data)
    {
        std::rotate(b_data, p_data, p_data + esize);
        b_data += w * esize;
        p_data += esize;
    }
}

int sub_gather_merge(bool root, Buffer &local_buf, const void *data, int esize, const Grid &grid)
{
    const auto[h, w] = grid.shape;
    const auto[vert, hori] = grid.dims;
    const auto pipeline_size = (w - 1) + (h - 1);
    const auto block_size = (w - 1) * (h - 1);

    int elems_to_send = 0;
    // Add the data from the current node to the buffer
    copy_at(local_buf, elems_to_send, data, esize);
    elems_to_send += 1;

    // Schedule data collection from recursive block
    std::future<void> fut = {};
    if (h != 0)
    {
        fut = grid.Irecv(at(local_buf, elems_to_send, esize), block_size * esize, hori);
        elems_to_send += block_size;
    }

    // Synchronously collect data from iterative pipeline
    if (w != 0)
    {
        for (int i = 0; i < pipeline_size; i++)
        {
            grid.Recv(at(local_buf, elems_to_send, esize), esize, vert);
            elems_to_send += 1;
        }
    }

    // Wait for the asynchronous block collection to complete
    if (h != 0 && fut.valid())
    { fut.wait(); }

    // Fix the order of the elements in the array we constructed
    fix_merge_order(local_buf, esize, grid);

    // Send all the collected data to the left parent
    if (!root)
    { grid.Bsend(at(local_buf, 0, esize), elems_to_send * esize, hori); }

    return elems_to_send;
}

int sub_gather_pipeline(Buffer &local_buf, const void *data, int esize, const Grid &grid)
{
    const auto[y, x] = grid.coords;
    const auto[h, w] = grid.shape;
    const auto[vert, hori] = grid.dims;

    const bool is_left = (x == 0);
    const bool is_bottom = (y == h - 1);

    const auto pipeline_size = (w - x - 1) + (h - y - 1);
    const auto send_direction = is_left ? vert : hori;
    const auto recv_direction = is_bottom ? hori : vert;

    int sent_elems = 0;

    // Send out own element first
    copy_at(local_buf, sent_elems, data, esize);
    grid.Bsend(at(local_buf, sent_elems, esize), esize, send_direction);
    sent_elems += 1;

    // Then, for each remaining element in the pipeline, passthrough its data
    while (sent_elems < pipeline_size + 1)
    {
        grid.Recv(at(local_buf, sent_elems, esize), esize, recv_direction);
        grid.Bsend(at(local_buf, sent_elems, esize), esize, send_direction);
        sent_elems += 1;
    }

    return sent_elems;
}

int sub_gather(bool root, Buffer &local_buf, const void *data, int esize, const Grid &grid)
{
    const auto[y, x] = grid.coords;
    const auto[h, w] = grid.shape;
    (void) w;

    const bool is_left = (x == 0);
    const bool is_top = (y == 0);
    const bool is_bottom = (y == h - 1);

    if (is_top && is_left)
    {
        // In merging point for current grid
        return sub_gather_merge(root, local_buf, data, esize, grid);
    }
    else if (is_left || is_bottom)
    {
        // In synchronous pipeline segment
        return sub_gather_pipeline(local_buf, data, esize, grid);
    }
    else
    {
        // In sub-grid, solve recursively
        return sub_gather(
            false, local_buf, data, esize,
            grid.shift_view(0, -1, -1, -1)
        );
    }
}

void gather(const void *sendbuf, void *recvbuf, int esize, Grid grid)
{
    const auto[y, x] = grid.coords;
    const auto[h, w] = grid.shape;
    Buffer local_buf = Buffer(esize * h * w);

    // Swap axes if needed
    if (w > h)
    { grid = grid.transpose(); }

    // Do the gather operation
    sub_gather(true, local_buf, sendbuf, esize, grid);
    MPI_Barrier(grid.grid_comm);

    if ((x == 0) && (y == 0))
    {
        if (w > h)
        { transpose_memcpy(recvbuf, local_buf, esize, h, w); }
        else
        { memcpy(recvbuf, local_buf.data(), esize * h * w); }
    }
}
