
#include "../inc/grid.hpp"

#include "mpi_common.hpp"


Grid::Grid(int h, int w) :
    comm_rank{},
    comm_size{},
    coords{},
    shape{h, w},
    dims{0, 1},
    grid_comm{},
    view{false}
{
    constexpr int periods[] = {0, 0};
    int world_size;

    MPI_Comm_size(comm_world, &world_size);
    if (world_size != h * w)
    {
        mpi_master_cout << "This program was started with " << world_size
                        << " processes, however a Grid of shape (" << h << ", " << w
                        << ") was requested, which requires *exactly* " << h * w
                        << " processes.\n" << sync_done;
        fini(-1);
    }

    MPI_Cart_create(comm_world, 2, shape, periods, true, &grid_comm);
    MPI_Comm_size(grid_comm, &comm_size);
    MPI_Comm_rank(grid_comm, &comm_rank);
    MPI_Cart_coords(grid_comm, comm_rank, 2, coords);
}

Grid::Grid(const Grid &grid) noexcept:
    comm_rank{grid.comm_rank},
    comm_size{grid.comm_size},
    coords{grid.coords[0], grid.coords[1]},
    shape{grid.shape[0], grid.shape[1]},
    dims{grid.dims[0], grid.dims[1]},
    grid_comm{grid.grid_comm},
    view{true}
{}

Grid &Grid::operator=(Grid &&src) noexcept
{
    comm_rank = src.comm_rank;
    comm_size = src.comm_size;

    coords[0] = src.coords[0];
    coords[1] = src.coords[1];

    shape[0] = src.shape[0];
    shape[1] = src.shape[1];

    dims[0] = src.dims[0];
    dims[1] = src.dims[1];

    grid_comm = src.grid_comm;

    view = src.view;
    src.view = true;
    return *this;
}

Grid::~Grid()
{
    if (!view)
    {
        MPI_Barrier(grid_comm);
        MPI_Comm_free(&grid_comm);
    }
}

Grid Grid::transpose() const noexcept
{
    Grid transposed = *this;
    transposed.coords[0] = coords[1];
    transposed.coords[1] = coords[0];
    transposed.shape[0] = shape[1];
    transposed.shape[1] = shape[0];
    transposed.dims[0] = dims[1];
    transposed.dims[1] = dims[0];
    return transposed;
}

Grid Grid::shift_view(int x, int y, int h, int w) const noexcept
{
    Grid shifted = *this;
    shifted.coords[0] += x;
    shifted.coords[1] += y;
    shifted.shape[0] += h;
    shifted.shape[1] += w;
    return shifted;
}

void Grid::Bsend(void *buf, int size, int direction) const
{
    int src, dest;
    MPI_Cart_shift(grid_comm, direction, -1, &src, &dest);
    timed_Bsend(buf, size, dest, 0, grid_comm);
}

void Grid::Recv(void *buf, int size, int direction) const
{
    int src, dest;
    MPI_Cart_shift(grid_comm, direction, 1, &dest, &src);
    timed_Recv(buf, size, src, MPI_ANY_TAG, grid_comm);
}

std::future<void> Grid::Irecv(void *buf, int size, int direction) const
{
    int src, dest;
    MPI_Cart_shift(grid_comm, direction, 1, &dest, &src);
    return timed_Irecv(buf, size, src, MPI_ANY_TAG, grid_comm);
}

