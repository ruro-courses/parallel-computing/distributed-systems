#include <iostream>

#include "mpi_common.hpp"
#include "grid.hpp"
#include "gather.hpp"


constexpr auto msg_size = 4;
using Message = char[msg_size];

void test_gather(const Grid &grid)
{
    MPI_Barrier(comm_world);
    const auto[y, x] = grid.coords;
    const auto[h, w] = grid.shape;
    const auto esize = sizeof(Message);

    // Allocate an arena buffer, that will be used for the buffered send
    Arena msg_arena = Arena(esize, h * w);

    // Create messages, that we want to gather
    Message local_data;
    snprintf(local_data, esize, "%d %d", y, x);
    local_data[esize - 1] = ',';

    // Create a receive buffer and perform the gather
    bool gather_target = (x == 0) && (y == 0);
    Buffer recv_buffer = Buffer(gather_target ? (esize * h * w) : 0);

    gather(local_data, recv_buffer.data(), esize, grid);

    // Clean up
    MPI_Barrier(comm_world);
    if (!gather_target)
    { return; }

    // Print the gathered messages
    mpi_master_cout << "Node (0, 0) is printing the gathered data:\n";
    for (auto i = 0; i < h; i++)
    {
        auto i_off = i * w * sizeof(Message);
        for (auto j = 0; j < w; j++)
        {
            auto j_off = j * sizeof(Message);
            for (std::size_t c = 0; c < sizeof(Message); c++)
            { mpi_master_cout << recv_buffer[c + j_off + i_off]; }
            mpi_master_cout << ' ';
        }
        mpi_master_cout << '\n';
    }
    mpi_master_cout << "Done.\n\n" << sync_done;
}

int parse_number(char *arg, const char *name)
{
    char *end;
    unsigned long num = strtoul(arg, &end, 10);
    if (*end != '\0')
    {
        mpi_master_cout << arg << " (" << name << ") is not a valid number.\n" << sync_done;
        fini(-1);
    }
    return static_cast<int>(num);
}

static int num_trials, grid_width, grid_height;

void read_args(int argc, char *argv[])
{
    if (argc != 4)
    {
        mpi_master_cout << "Usage: " << argv[0]
                        << " [num_trials] [grid_width] [grid_height]\n" << sync_done;
        fini(-1);
    }
    num_trials = parse_number(argv[1], "num_trials");
    grid_width = parse_number(argv[2], "grid_width");
    grid_height = parse_number(argv[3], "grid_height");
    mpi_master_cout << "Running " << num_trials << " trials on a grid of shape ("
                    << grid_width << ", " << grid_height << ").\n" << sync_done;
}

int main(int argc, char *argv[])
{
    try
    {
        init(&argc, &argv);
        read_args(argc, argv);

        {
            // Create the process grid
            Grid grid = {grid_height, grid_width};
            auto times = std::vector<double>(num_trials);
            for (auto ind = 0; ind < num_trials; ind++)
            {
                double t_start = MPI_Wtime();
                asm volatile ("":: : "memory"); // Please, don't reorder this line

                test_gather(grid); // Run the timed test

                asm volatile ("":: : "memory"); // Please, don't reorder this line
                double t_end = MPI_Wtime();

                times[ind] = t_end - t_start;
            }

            // Output the timing stats
            mpi_master_cout << "\n[";
            for (auto ind = 0; ind < num_trials; ind++)
            { mpi_master_cout << times[ind] << ", "; }
            mpi_master_cout << "] # times\n" << sync_done;
        }
    }
    catch (std::exception &e)
    {
        std::cerr << e.what() << '\n';
        fini(-1);
    }
    fini(0);
}
