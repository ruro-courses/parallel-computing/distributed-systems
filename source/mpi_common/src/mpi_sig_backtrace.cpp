#include "mpi_sig_backtrace.hpp"
#include "mpi_world.hpp"

#include <cstdio>
#include <csignal>
#include <execinfo.h>


void handler(int sig)
{
    // Collect current backtraces
    void *array[32];
    size_t bt_size;
    bt_size = backtrace(array, sizeof(array) / sizeof(array[0]));
    char **bts = backtrace_symbols(array, bt_size);

    // Setup exclusive execution block
    int rank;
    MPI_Comm_rank(comm_world, &rank);

    // Print backtraces
    fprintf(stderr, "Process %d was interrupted by signal %d...\n", rank, sig);
    if (!bts)
    { fprintf(stderr, "Process %d has no backtrace!\n", rank); }
    else
    {
        fprintf(stderr, "Process %d backtrace:\n", rank);
        for (int i = 0; bts[i]; i++)
        { fprintf(stderr, "%s\n", bts[i]); }
        fprintf(stderr, "\n");
    }
    fflush(stderr);

    // Shutdown
    fini(-1);
}

void install_handlers()
{
    // Install backtrace handlers
    signal(SIGSEGV, handler);
    signal(SIGABRT, handler);
    signal(SIGQUIT, handler);
    signal(SIGBUS, handler);
    signal(SIGILL, handler);
    signal(SIGFPE, handler);
}

