#include "mpi_arena.hpp"

#include <mpi.h>


Arena::Arena(size_t message_size, int message_count) : buffer{}
{
    size_t size = message_count * (MPI_BSEND_OVERHEAD + message_size);
    buffer.resize(size);
    MPI_Buffer_attach(buffer.data(), buffer.size());
}

Arena::~Arena()
{
    int size = buffer.size();
    MPI_Buffer_detach(buffer.data(), &size);
    buffer.clear();
}

