#include "mpi_timed_ops.hpp"


void wait(int bytes)
{
#ifdef SYNTHETIC_TIMING
    std::this_thread::sleep_for(
        time_unit{
            time_scale * (time_send_init + bytes * time_send_byte)
        }
    );
#else
    (void) bytes;
#endif
}

void timed_Bsend(const void *buf, int count, int dest, int tag, MPI_Comm comm)
{
    // Buffered send is async
    MPI_Bsend(buf, count, MPI_BYTE, dest, tag, comm);
}

void timed_Recv(void *buf, int count, int source, int tag, MPI_Comm comm)
{
    // Regular receive is synchronized
    MPI_Recv(buf, count, MPI_BYTE, source, tag, comm, MPI_STATUS_IGNORE);
    wait(count);
}

std::future<void> timed_Irecv(void *buf, int count, int source, int tag, MPI_Comm comm)
{
    // Non-blocking receive is synchronized, but the wait can be async delayed
    // until the point, where future.wait() is called.
    return std::async(
        std::launch::async,
        [=]()
        { timed_Recv(buf, count, source, tag, comm); }
    );
}
