#include "mpi_stream_helpers.hpp"


MPISyncOstream::MPISyncOstream(std::ostream *_str, MPI_Comm _comm, bool _active, bool _exclusive) noexcept
    : buffer(), str(_str), comm(_comm), active(_active), exclusive(_exclusive), blocking(), size(), rank()
{
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);
}

MPISyncOstream &MPISyncOstream::operator<<(std::ostream &(*manip)(std::ostream &))
{
    if (str && active)
    {
        enter();
        buffer << manip;
    }
    return *this;
}

void MPISyncOstream::enter()
{
    if (exclusive && !blocking)
    {
        for (int i = 0; i < rank; i++)
        { MPI_Barrier(comm); }
        blocking = true;
    }
}

void MPISyncOstream::exit()
{
    if (exclusive && blocking)
    {
        for (int i = rank; i < size; i++)
        { MPI_Barrier(comm); }
        MPI_Barrier(comm);
        blocking = false;
    }
}

MPISyncOstream::MPISyncOstream() noexcept
    : buffer(), str(nullptr), comm(MPI_COMM_WORLD), active(false), exclusive(false), blocking(false), size(0), rank(0)
{}

void MPISyncOstream::reindex(MPI_Comm new_comm)
{
    comm = new_comm;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);
    blocking = false;
    if (str && active)
    { std::flush(*str); }
}

MPISyncOstream mpi_master_cout{};
MPISyncOstream mpi_common_cerr{};
