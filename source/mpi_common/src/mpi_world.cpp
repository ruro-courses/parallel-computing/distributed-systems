#include "mpi_world.hpp"
#include "mpi_sig_backtrace.hpp"
#include "mpi_stream_helpers.hpp"

#include <fstream>


#ifdef DEBUG
std::ofstream logfile = {}; // NOLINT(cert-err58-cpp)
#endif

MPI_Comm comm_world = MPI_COMM_WORLD;

void init(int *argc, char **argv[], MPI_Comm_errhandler_function error_handler_function)
{
    install_handlers();
    int thread_support = MPI_THREAD_SINGLE;
    MPI_Init_thread(argc, argv, MPI_THREAD_MULTIPLE, &thread_support);
    if (thread_support < MPI_THREAD_MULTIPLE)
    {
        throw std::runtime_error(
            "Couldn't obtain MPI_THREAD_MULTIPLE level support. "
            "This program requires a thread-safe MPI implementation. "
            "Please recompile your MPI provider with thread support."
        );
    }

    if (error_handler_function)
    {
        MPI_Errhandler error_handler;
        MPI_Comm_create_errhandler(error_handler_function, &error_handler);
        MPI_Comm_set_errhandler(comm_world, error_handler);
    }
    else
    { MPI_Comm_set_errhandler(comm_world, MPI_ERRORS_ARE_FATAL); }

    int rank;
    MPI_Comm_rank(comm_world, &rank);
    mpi_master_cout = MPISyncOstream{&std::cout, comm_world, rank == 0, false};

#ifdef DEBUG
    std::string logname = "rank" + std::to_string(rank) + ".log";
    logfile.open(logname);
    mpi_common_cerr = {&logfile, comm_world, true, false};
#else
    mpi_common_cerr = MPISyncOstream{&std::cerr, comm_world, true, false};
#endif
}

[[noreturn]] void fini(int status)
{
    int rank;
    MPI_Comm_rank(comm_world, &rank);

#ifdef DEBUG
    std::flush(logfile);
    logfile.close();
#endif

    if (!status)
    { MPI_Barrier(comm_world); }

    MPI_Finalize();
    if (rank != 0)
    { exit(0); }
    else
    { exit(status); }
}
