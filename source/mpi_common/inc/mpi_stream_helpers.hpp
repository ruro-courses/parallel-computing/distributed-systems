#pragma once


#include <mpi.h>
#include <iostream>
#include <sstream>


enum SYNC_DONE_MARKER
{
    sync_done_marker
};
constexpr auto sync_done = SYNC_DONE_MARKER::sync_done_marker;

template<class T>
using expr_type = std::remove_cv_t<std::remove_reference_t<T>>;

template<class T, class U>
constexpr auto same_type = std::is_same_v<expr_type<T>, expr_type<U>>;


class MPISyncOstream
{
public:
    explicit MPISyncOstream() noexcept;
    explicit MPISyncOstream(std::ostream *_str, MPI_Comm _comm, bool _active, bool _exclusive = true) noexcept;

    template<class T>
    MPISyncOstream &operator<<(T &&x)
    {
        if (str && active)
        {
            enter();
            if (same_type<T, SYNC_DONE_MARKER>)
            {
                *str << buffer.str();
                std::flush(*str);
                buffer.str("");
                buffer.clear();
                exit();
            }
            else
            { buffer << std::forward<T>(x); }
        }
        return *this;
    }

    MPISyncOstream &operator<<(std::ostream &(*manip)(std::ostream &));
    void reindex(MPI_Comm new_comm);
private:
    void enter();
    void exit();

    std::ostringstream buffer;
    std::ostream *str;
    MPI_Comm comm;
    bool active;
    bool exclusive;
    bool blocking;

    int size;
    int rank;
};


extern MPISyncOstream mpi_master_cout;
extern MPISyncOstream mpi_common_cerr;
