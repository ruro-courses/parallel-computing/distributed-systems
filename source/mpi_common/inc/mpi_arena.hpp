#pragma once


#include <vector>


using Buffer = std::vector<char>;


class Arena
{
public:
    Arena(size_t message_size, int message_count);
    ~Arena();
private:
    Buffer buffer;
};
