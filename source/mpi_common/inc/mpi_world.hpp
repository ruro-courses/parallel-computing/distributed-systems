#pragma once


#include <mpi.h>


extern MPI_Comm comm_world;
void init(int *argc, char **argv[], MPI_Comm_errhandler_function error_handler_function = nullptr);
[[noreturn]] void fini(int status);
