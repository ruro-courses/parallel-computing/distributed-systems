#pragma once


#include <mpi.h>
#include <future>


using time_unit = std::chrono::milliseconds;
constexpr auto time_scale = 10;
constexpr auto time_send_init = 100;
constexpr auto time_send_byte = 1;

void timed_Bsend(const void *buf, int count, int dest, int tag, MPI_Comm comm);
void timed_Recv(void *buf, int count, int source, int tag, MPI_Comm comm);
std::future<void> timed_Irecv(void *buf, int count, int source, int tag, MPI_Comm comm);
