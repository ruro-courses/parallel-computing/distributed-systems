#!/bin/sh
set -euo pipefail
cd "$(dirname "$0")"

mkdir -p ../build
cd ../build
cmake $@ ../source
make
