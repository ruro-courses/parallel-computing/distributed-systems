#!/bin/sh
set -euo pipefail
cd "$(dirname "$0")"

PROJECT_DIR=$(realpath "${PWD}/..")
BUILD_DIR="${PROJECT_DIR}/build"

docker run                                   \
    -v ${BUILD_DIR}:/sandbox/build           \
    -w /sandbox/build ulfm                   \
    mpirun --oversubscribe -mca btl tcp,self \
    $@
