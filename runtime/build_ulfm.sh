#!/bin/sh
set -euo pipefail
cd "$(dirname "$0")"

docker build -t ulfm .
