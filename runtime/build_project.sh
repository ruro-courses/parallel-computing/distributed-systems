#!/bin/sh
set -euo pipefail
cd "$(dirname "$0")"

PROJECT_DIR=$(realpath "${PWD}/..")
docker run                                   \
    -v ${PROJECT_DIR}:/sandbox/distr_systems \
    -w /sandbox/distr_systems ulfm           \
    runtime/build_source.sh $@
