#!/bin/sh
set -euo pipefail
cd "$(dirname "$0")"

NUM_PROCESSES=${1:-10}
runtime/run.sh -np ${NUM_PROCESSES} safe_bcast/safe_bcast
