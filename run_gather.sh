#!/bin/sh
set -euo pipefail
cd "$(dirname "$0")"

GRID_SIZE_X=${1:-4}
GRID_SIZE_Y=${2:-4}
NUM_TRIALS=${3:-10}
runtime/run.sh -q -np $((${GRID_SIZE_X} * ${GRID_SIZE_Y})) gather/gather ${NUM_TRIALS} ${GRID_SIZE_X} ${GRID_SIZE_Y}
